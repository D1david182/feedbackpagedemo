//
//  ViewController.swift
//  Peachly Mobile
//
//  Created by Daniel Davidson on 8/14/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {
    
    let CharacterLimit = 500
    let messagePlaceHolder = "Message..."
    let datasource = DataSource()
    
    let headerLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = purpleTextColor
        lbl.font = UIFont.systemFont(ofSize: 26, weight: .bold)
        lbl.textAlignment = .center
        lbl.text = "Feedback"
        return lbl
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = primaryGray
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 6
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        return view
    }()
    
    lazy var firstNameEntry : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 3
        view.attributedPlaceholder = NSAttributedString(string: " First Name", attributes: [.foregroundColor: UIColor.lightGray])
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = primaryBlack
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 3
        view.clipsToBounds = false
        view.tintColor = primaryBlack
        return view
    }()
    
    lazy var lastNameEntry : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 3
        view.attributedPlaceholder = NSAttributedString(string: " Last Name", attributes: [.foregroundColor: UIColor.lightGray])
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = primaryBlack
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 3
        view.clipsToBounds = false
        view.tintColor = primaryBlack
        return view
    }()
    
    lazy var emailEntry : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 3
        view.attributedPlaceholder = NSAttributedString(string: " john@example.com", attributes: [.foregroundColor: UIColor.lightGray])
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = primaryBlack
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 3
        view.clipsToBounds = false
        view.tintColor = primaryBlack
        return view
    }()
    
    lazy var messageEntry : UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 3
        view.text = messagePlaceHolder
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = UIColor.lightGray
        view.tintColor = primaryBlack
        view.delegate = self
        view.layer.masksToBounds = true
        return view
    }()
    
    let messageShadowView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 3
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 3
        view.backgroundColor = .white
        return view
    }()
    
    let letterCountLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        lbl.text = "0/500"
        return lbl
    }()
    
    lazy var submitButton : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Submit", for: .normal)
        btn.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        btn.backgroundColor = purpleButtonColor
        btn.layer.cornerRadius = 3
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.layer.shadowOpacity = 0.2
        btn.layer.shadowRadius = 3
        return btn
    }()
    
    let loadingWheel : UIActivityIndicatorView = {
       let wheel = UIActivityIndicatorView(style: .whiteLarge)
        wheel.translatesAutoresizingMaskIntoConstraints = false
        wheel.isHidden = true
        wheel.color = purpleButtonColor
        return wheel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.setGradientBackground(colorOne: secondaryBlack, colorTwo: primaryBlack)
        setupComponents()
        addConstraints()
    }
    
    private func setupComponents(){
        view.addSubview(containerView)
        containerView.addSubview(headerLabel)
        containerView.addSubview(firstNameEntry)
        containerView.addSubview(lastNameEntry)
        containerView.addSubview(emailEntry)
        containerView.addSubview(messageShadowView)
        containerView.addSubview(messageEntry)
        containerView.addSubview(letterCountLabel)
        containerView.addSubview(submitButton)
        view.addSubview(loadingWheel)
    }
    private func addConstraints(){
        containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        
        headerLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20).isActive = true
        headerLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        
        firstNameEntry.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        firstNameEntry.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        firstNameEntry.heightAnchor.constraint(equalToConstant: 35).isActive = true
        firstNameEntry.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 20).isActive = true
        
        lastNameEntry.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        lastNameEntry.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        lastNameEntry.heightAnchor.constraint(equalToConstant: 35).isActive = true
        lastNameEntry.topAnchor.constraint(equalTo: firstNameEntry.bottomAnchor, constant: 20).isActive = true
        
        emailEntry.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        emailEntry.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        emailEntry.heightAnchor.constraint(equalToConstant: 35).isActive = true
        emailEntry.topAnchor.constraint(equalTo: lastNameEntry.bottomAnchor, constant: 20).isActive = true
        
        messageEntry.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        messageEntry.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        messageEntry.heightAnchor.constraint(equalToConstant: 100).isActive = true
        messageEntry.topAnchor.constraint(equalTo: emailEntry.bottomAnchor, constant: 20).isActive = true
        
        messageShadowView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        messageShadowView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        messageShadowView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        messageShadowView.topAnchor.constraint(equalTo: emailEntry.bottomAnchor, constant: 20).isActive = true
        
        letterCountLabel.topAnchor.constraint(equalTo: messageEntry.bottomAnchor, constant: 5).isActive = true
        letterCountLabel.leadingAnchor.constraint(equalTo: messageEntry.leadingAnchor).isActive = true
        
        submitButton.topAnchor.constraint(equalTo: letterCountLabel.bottomAnchor, constant: 25).isActive = true
        submitButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12).isActive = true
        submitButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        loadingWheel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingWheel.topAnchor.constraint(equalTo: submitButton.bottomAnchor, constant: 20).isActive = true
        
    }
    @objc func handleSubmit(sender: UIButton){
        view.endEditing(true)
        //Check if all values are present and valid
        guard let email = emailEntry.text, let firstName = firstNameEntry.text, let lastName = lastNameEntry.text, let message = messageEntry.text else {return}
        
        if email.trimmingCharacters(in: .whitespaces).isEmpty || firstName.trimmingCharacters(in: .whitespaces).isEmpty || lastName.trimmingCharacters(in: .whitespaces).isEmpty || message.trimmingCharacters(in: .whitespaces).isEmpty || message == messagePlaceHolder{
            sender.shake()
            showAlert("Error", "Make sure you fill out all fields.")
            return
        }
        if !emailIsValid(email: email){
            sender.shake()
            showAlert("Error", "Email is not valid.")
            return
        }
        
        //All values are valid, post to server
        sender.pulsate()
        loadingWheel.startAnimating()
        loadingWheel.isHidden = false
        let perameters = [
            "first" : firstName,
            "last" : lastName,
            "email" : email,
            "message" : message
        ]
        datasource.postToServer(perameters: perameters) { (response, error) in
            self.stopLoadingWheel()
            if let err = error{
                print("Error in post response", err)
                return
            }
            print(response!)
        }
    }
    
    //Helper functions
    private func stopLoadingWheel(){
        DispatchQueue.main.async {
             self.loadingWheel.stopAnimating()
        }
    }
    private func emailIsValid(email : String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}


extension FeedbackViewController: UITextViewDelegate {
    
    //Used to allow a placeholder message on a uitextview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = primaryBlack
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = messagePlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }
    //Update and enforce character limit
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        letterCountLabel.text = "\(updatedText.count)/500"
        return updatedText.count <= CharacterLimit - 1
    }
}
