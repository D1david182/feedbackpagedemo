//
//  ConstantsAndHelpers.swift
//  Peachly Mobile
//
//  Created by Daniel Davidson on 8/14/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

//#2a2a2a
let secondaryBlack = UIColor(red:0.16, green:0.16, blue:0.16, alpha:1.0)

//#191919
let primaryBlack = UIColor(red:0.10, green:0.10, blue:0.10, alpha:1.0)

//#e7e7e7
let primaryGray = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)

//#bb8ce5
let purpleTextColor = UIColor(red:0.73, green:0.55, blue:0.90, alpha:1.0)

//#d3a1ff
let purpleButtonColor = UIColor(red:0.83, green:0.63, blue:1.00, alpha:1.0)


extension UIView {
    
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.15
        pulse.fromValue = 0.8
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 0
        pulse.initialVelocity = 1
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.05
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 3, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 3, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}
extension UIViewController {
    public func showAlert(_ title:String, _ message:String) {
        let alertVC = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .actionSheet)
        let okAction = UIAlertAction(
            title: "OK",
            style: .cancel,
            handler: { action -> Void in
        })
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
}
