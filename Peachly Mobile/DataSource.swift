//
//  DataSource.swift
//  Peachly Mobile
//
//  Created by Daniel Davidson on 8/15/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import Foundation
import UIKit

struct PostResponse: Decodable{
    let first : String
    let last : String
    let email : String
    let message: String
}

class DataSource: NSObject {
    
    func postToServer(perameters : [String : Any], completion : @escaping (PostResponse?, Error?) ->()){
        
        let urlString = "http://52.15.184.142:80/feedback"
        guard let url = URL.init(string: urlString) else {return}
        
        //Build request object
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        
        //Attach perameters
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: perameters, options: .prettyPrinted)
        } catch let error {
            print("Invalid perameters", error)
            completion(nil, error)
            return
        }
        //Post to server
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let err = error{
                print("Error in service request call" , err)
                completion(nil, error)
                return
            }
            guard let data = data else {return}
            do {
                
                let description = try JSONDecoder().decode(PostResponse.self, from: data)
                completion(description, nil)
                
            }catch{
                print("Error in service request call", error)
                completion(nil, error)
            }
            
            }.resume()
    }
}
